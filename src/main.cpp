#include <Arduino.h>
#include <Servo.h>
// #include "IRremote.h"
// #include "ir_command_codes.h"

// EN: Analog pin where IR detector is pluged in.
// RU: Аналоговый вход контроллера, к которому подключен ИК-приёмник.
// int irPin = A0;
int lazerPin = 6;
int servoPin = 7;
unsigned long timePoint = 0;
unsigned long timePointSonic = 0;
int servoDeg = 90;
int frontLed = 3;
int backLed = 4;
int trig = 4;
int echo = 5;
int val;
unsigned int impulseTime = 0;
unsigned int distance_sm = 0;
Servo servo;

struct Motor
{
  int pinA;
  int pinB;
  int pinSpeed;
};

template <typename T, size_t N>
boolean hasCode(T (&commandCodes)[N], long code)
{
  for (int i = 0; i < N; i++)
  {
    if (commandCodes[i] == code)
    {
      return true;
    }
  }
  return false;
}

void forward(Motor *left, Motor *right)
{
  digitalWrite(left->pinA, HIGH);
  digitalWrite(left->pinB, LOW);
  digitalWrite(right->pinA, LOW);
  digitalWrite(right->pinB, HIGH);
  digitalWrite(frontLed, HIGH);
  digitalWrite(backLed, LOW);
}

void backward(Motor *left, Motor *right)
{
  digitalWrite(left->pinA, LOW);
  digitalWrite(left->pinB, HIGH);
  digitalWrite(right->pinA, HIGH);
  digitalWrite(right->pinB, LOW);
  digitalWrite(frontLed, LOW);
  digitalWrite(backLed, HIGH);
}

void stop(Motor *left, Motor *right)
{
  digitalWrite(left->pinA, LOW);
  digitalWrite(left->pinB, LOW);
  digitalWrite(right->pinA, LOW);
  digitalWrite(right->pinB, LOW);
}

void rotateLeft(Motor *left, Motor *right)
{
  digitalWrite(left->pinA, LOW);
  digitalWrite(left->pinB, HIGH);
  digitalWrite(right->pinA, LOW);
  digitalWrite(right->pinB, HIGH);
}

void rotateRigh(Motor *left, Motor *right)
{
  digitalWrite(left->pinA, HIGH);
  digitalWrite(left->pinB, LOW);
  digitalWrite(right->pinA, HIGH);
  digitalWrite(right->pinB, LOW);
}

void fire()
{
  digitalWrite(lazerPin, HIGH);
  timePoint = millis();
}

void stopFire()
{
  digitalWrite(lazerPin, LOW);
}

Motor left = {8, 9};
Motor right = {11, 10};
void setup()
{
  servo.attach(servoPin);

  pinMode(lazerPin, OUTPUT);
  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  Serial.begin(9600);

  servo.write(0);
  delay(500);
  servo.write(180);
  delay(500);
  servo.write(servoDeg);
}

void loop()
{
  // RU: Отключаем лазер ЕСЛИ временная метка установлена и больше текущего времени на 1000 мс
  if (timePoint != 0 && (timePoint + 500) <= millis())
  {
    timePoint = 0;
    stopFire();
  }

  if (Serial.available())
  {
    val = Serial.read();
    if (val == '1')
    {
      if (distance_sm < 10)
      {
        return;
      }
      forward(&left, &right);
    }
    else if (val == '2')
    {
      backward(&left, &right);
    }
    else if (val == '6' || distance_sm < 10)
    {
      stop(&left, &right);
    }
    else if (val == '3')
    {
      rotateLeft(&left, &right);
    }
    else if (val == '4')
    {
      rotateRigh(&left, &right);
    }
    else if (val == '5')
    {
      if (servoDeg <= 10)
      {
        return;
      }
      servoDeg -= 5;
    }
    else if (val == '8')
    {
      if (servoDeg >= 170)
      {
        return;
      }
      servoDeg += 5;
    }
    servo.write(servoDeg);
    if (val == '7')
    {
      fire();
    }
  }

  digitalWrite(trig, HIGH);
  if ((timePointSonic + 20) <= millis())
  {
    timePointSonic = millis();
    digitalWrite(trig, LOW);           // перестаем подавать импульс на пин Trig ультразвукового датчика
    impulseTime = pulseIn(echo, HIGH); // Замеряем длину отраженного импульса
    distance_sm = impulseTime / 58;    // Переводим полученные данные в сантиметры
    if (!distance_sm)
    {
      distance_sm = 20;
    }
    Serial.println(distance_sm);
    Serial.println("-------");
  }
}