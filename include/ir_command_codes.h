// EN: Сommand codes received from IR remote. One command can be assigned to many codes.
//     You can type codes from several IR remotes.
// RU: Коды команд, принимаемых от ИК-пульта. Одна команда может вызываться несколькими
//     кодами. Вы можете ввести коды от нескольких ИК-пультов.
const long IR_COMMAND_FORWARD_CODES[] = {16718055};    // вперёд
const long IR_COMMAND_BACKWARD_CODES[] = {16730805};   //назад
const long IR_COMMAND_TURN_LEFT_CODES[] = {16716015};  // поворот влево
const long IR_COMMAND_TURN_RIGHT_CODES[] = {16734885}; // поворот вправо
const long IR_COMMAND_FORWARD_LEFT_CODES[] = {1};
const long IR_COMMAND_FORWARD_RIGHT_CODES[] = {3};

const long IR_COMMAND_TOWER_TURN_LEFT_CODES[] = {16712445};  // Поворот башни влево
const long IR_COMMAND_TOWER_TURN_RIGHT_CODES[] = {16720605}; // Поворот башни вправо

const long IR_COMMAND_STOP_CODES[] = {16726215}; // Стоп
// const long IR_COMMAND_SLOW_CODES[] = {617287, 2091, 43, 16769055}; // Медленнее
// const long IR_COMMAND_FAST_CODES[] = {92999, 2074, 26, 16754775}; // Быстрее
const long IR_COMMAND_FIRE_CODES[] = {16748655}; // Огонь
